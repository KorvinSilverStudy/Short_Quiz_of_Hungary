/*
 * Copyright 2017, Korvin F. Ezüst
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.korvin.shortquizofhungary;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    int selectedCoatOfArms = 0;
    int score              = 0;
    int scoreMax           = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * This is called when the first ImageButton is clicked
     */
    public void setFirst(View v)
    {
        setOnlyOneButton(true, false, false, false);
    }

    /**
     * This is called when the second ImageButton is clicked
     */
    public void setSecond(View v)
    {
        setOnlyOneButton(false, true, false, false);
    }

    /**
     * This is called when the third ImageButton is clicked
     */
    public void setThird(View v)
    {
        setOnlyOneButton(false, false, true, false);
    }

    /**
     * This is called when the fourth ImageButton is clicked
     */
    public void setFourth(View v)
    {
        setOnlyOneButton(false, false, false, true);
    }

    /**
     * Make sure only one ImageButton stays pressed.
     * Based on which was one pressed last, this method keeps it pressed
     * while "unpresses" the other three.
     * Changes the pressed buttons image to its "checked" image
     * and changes the other three's to their "unchecked" images.
     * Last it sets the selectedCoatOfArms variable's value to
     * the year number of the pressed ImageButton's image.
     * The images are three historical and one current coat of arm
     * first came into usage in 1918, 1946, 1957 and 1990, respectively.
     *
     * @param b1 true if the first ImageButton is pressed
     * @param b2 true if the second ImageButton is pressed
     * @param b3 true if the third ImageButton is pressed
     * @param b4 true if the fourth ImageButton is pressed
     */
    private void setOnlyOneButton(boolean b1, boolean b2, boolean b3, boolean b4)
    {
        ImageButton imageButton1918 = findViewById(R.id.imageButton1918); // 1st displayed image
        ImageButton imageButton1990 = findViewById(R.id.imageButton1990); // 2nd displayed image
        ImageButton imageButton1957 = findViewById(R.id.imageButton1957); // 3rd displayed image
        ImageButton imageButton1946 = findViewById(R.id.imageButton1946); // 4th displayed image

        imageButton1918.setPressed(false);
        imageButton1918.setImageResource(R.drawable.coat1918);
        imageButton1990.setPressed(false);
        imageButton1990.setImageResource(R.drawable.coat1990);
        imageButton1957.setPressed(false);
        imageButton1957.setImageResource(R.drawable.coat1957);
        imageButton1946.setPressed(false);
        imageButton1946.setImageResource(R.drawable.coat1946);

        if (b1)
        {
            imageButton1918.setPressed(true);
            imageButton1918.setImageResource(R.drawable.coat1918check);
            selectedCoatOfArms = 1918;
        }
        if (b2)
        {
            imageButton1990.setPressed(true);
            imageButton1990.setImageResource(R.drawable.coat1990check);
            selectedCoatOfArms = 1990;
        }
        if (b3)
        {
            imageButton1957.setPressed(true);
            imageButton1957.setImageResource(R.drawable.coat1957check);
            selectedCoatOfArms = 1957;
        }
        if (b4)
        {
            imageButton1946.setPressed(true);
            imageButton1946.setImageResource(R.drawable.coat1946check);
            selectedCoatOfArms = 1946;
        }
    }

    /**
     * Increments the score by one if the RadioButton is checked.
     *
     * @param id a RadioButton ID
     */
    private void incrementScoreRB(int id)
    {
        RadioButton radioButton = findViewById(id);
        if (radioButton.isChecked())
            score += 1;
    }

    /**
     * Increments the score by one if the correct and only the correct
     * CheckBoxes are checked.
     *
     * @param id1 first CheckBox
     * @param id2 second CheckBox
     * @param id3 third CheckBox
     * @param id4 fourth CheckBox
     * @param b1  true if the first CheckBox has correct answer, false otherwise
     * @param b2  true if the second CheckBox has correct answer, false otherwise
     * @param b3  true if the third CheckBox has correct answer, false otherwise
     * @param b4  true if the fourth CheckBox has correct answer, false otherwise
     */
    private void incrementScoreCB(
            int id1, int id2, int id3, int id4, boolean b1, boolean b2, boolean b3, boolean b4)
    {
        CheckBox c1 = findViewById(id1);
        CheckBox c2 = findViewById(id2);
        CheckBox c3 = findViewById(id3);
        CheckBox c4 = findViewById(id4);

        if (c1.isChecked() == b1
            && c2.isChecked() == b2
            && c3.isChecked() == b3
            && c4.isChecked() == b4)
            score += 1;
    }

    /**
     * Evaluates the answers after the submit button is checked,
     * "replaces" scrollView1 with scrollView2 and adds the final score
     * to its appropriate TextView.
     */
    public void submit(View v)
    {
        if (selectedCoatOfArms == 1990)
            score += 1;

        int[] correctRadioButtons = {R.id.rad1C, R.id.rad2C, R.id.rad3C};

        for (int item : correctRadioButtons)
            incrementScoreRB(item);

        incrementScoreCB(
                R.id.check4A1,
                R.id.check4A2,
                R.id.check4C1,
                R.id.check4C2,
                false,
                false,
                true,
                true);

        incrementScoreCB(
                R.id.check5A1,
                R.id.check5C1,
                R.id.check5C2,
                R.id.check5C3,
                false,
                true,
                true,
                true);

        incrementScoreCB(
                R.id.check6A1,
                R.id.check6C1,
                R.id.check6C2,
                R.id.check6C3,
                false,
                true,
                true,
                true);

        EditText editText = findViewById(R.id.text7);
        if (editText.getText().toString().equals("2004"))
            score += 1;

        editText = findViewById(R.id.text8);
        String string = editText.getText().toString().toLowerCase();
        if (string.equals("pengő") || string.equals("pengö") || string.equals("pengo"))
            score += 1;

        editText = findViewById(R.id.text9);
        string = editText.getText().toString();
        if (string.equals("895") || string.equals("896"))
            score += 1;

        ScrollView scrollView = findViewById(R.id.scrollView1);
        scrollView.setVisibility(View.GONE);
        scrollView = findViewById(R.id.scrollView2);
        scrollView.setVisibility(View.VISIBLE);

        TextView textView = findViewById(R.id.score);
        string = getString(R.string.score, score, scoreMax);
        textView.setText(string);

        Toast.makeText(getApplicationContext(), string, Toast.LENGTH_LONG).show();
    }
}
